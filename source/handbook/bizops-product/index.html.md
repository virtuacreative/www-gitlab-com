---
layout: markdown_page
title: "BizOps Product"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary

[BizOps](https://gitlab.com/bizops/bizops) is a convention-over-configuration framework for analytics, business intelligence, and data science. It leverages open source software and software development best practices including version control, CI, CD, and review apps.

## Team

* [Sid](https://gitlab.com/sytses)
* [Jacob](https://gitlab.com/jschatz1)
* [Micaël](https://gitlab.com/mbergeron)

## Code

See the active project here: [gitlab.com/bizops/bizops](https://gitlab.com/bizops/bizops)
