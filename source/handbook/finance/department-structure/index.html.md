--- 
layout: markdown_page
title: "Department Structure"
---

## Department Structure

Below is a table showing the structure of GitLab departments as they exist in NetSuite. Please [check out the Team Page](/team/chart) to see our org chart.

| Sales | Cost of Sales | Marketing | Development | General & Administrative | GitLab.com | GitHost |
| :-----------: | :------: | :------------: | :-----------: | :------: | :------------: | :------------: |
| Sales Management | Customer Solutions | Corporate Marketing | BizOps | PeopleOps | 
| Sales New Business Development | Customer Support  | Field Marketing | Infrastructure | Finance  ||
| Sales Account Management | |Growth Marketing | Product Management  | 
| Customer Success || Inbound Demand Generation  | 
| Indirect Channel || Outbound Demand Generation |||||
||| Outreach |||||
||| Product Marketing |||||

